<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');//登录框中间件
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //throw new \Exception("我故意的", 1);

        //向视图文件输出数据
        return view('home')->withArticles(\App\Article::all());
    }
    /*

        /{model}/lists 转发到{$model}Controller控制器的lists方法
        如 /shop/lists => shopController控制器 lists方法,
        /user/lists => userController控制器 lists方法,

        以下代码适用于 Laravel 5.0 及以上：
        Route::get('{model}/lists', function ($model) {
          $className = 'App\Http\Controllers\\'.ucfirst($model).'Controller';
          $obj = new $className;
          return $obj->lists();
        });
        });
     */
}
