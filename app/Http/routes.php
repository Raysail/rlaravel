<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* delete when lean leanlaravel5 3
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('now', function () {
    return date("Y-m-d H:i:s");
});

Route::auth();

//当以 GET 方法访问 http://www.ray.com:1024/home 的时候，调用 HomeController 控制器中的 index 方法（函数）。
//同理，你可以使用 Route::post('/home', 'HomeController@indexPost'); 响应 POST 方法的请求。
Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'auth', 'namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::get('/', 'HomeController@index');
    Route::resource('article', 'ArticleController');
});


